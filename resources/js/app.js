/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
import VueSweetalert2 from 'vue-sweetalert2';
import Vuex from 'vuex'
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.directive("csrf-token", {
    inserted: function(el) {
        let token = document.head.querySelector('meta[name="csrf-token"]').content;
        let hidden = document.createElement("input");
        hidden.setAttribute('type', "hidden")
        hidden.setAttribute('name', "_token");
        hidden.setAttribute('value', token);
        el.appendChild(hidden);
    }
});
Vue.use(VueSweetalert2);
Vue.use(Vuex)
Vue.component('user-list', require('./components/users/Users.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        users: []
    },
    mutations: {
        usersMutation (state, data) {
          state.users = data
        }
    },
    actions: {
        async getUsers( {commit}) {
            try {
                const userList = await axios.get("/v1/api/users");
                commit('usersMutation', userList.data)
            } catch (e) {
                alert(e);
            }
        }
    }
})

const app = new Vue({
    el: '#app',
    store: store
});
